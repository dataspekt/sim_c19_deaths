library(tidyverse)
library(jsonlite)
#library(zoo)

# Individual-level data (koronavirus.hr)

data_individual <-
    "https://www.koronavirus.hr/json/?action=po_osobama" %>%
    fromJSON() %>% as_tibble() %>%
    drop_na() %>%
    transmute(
        date = as.Date(Datum, format = "%Y-%m-%d"),
        age = as.numeric(format(date, "%Y")) - dob
    ) %>%
    arrange(date) %>%
    mutate(
        age_cat = case_when(
            age %in% 0:9 ~ "0-9",
            age %in% 10:19 ~ "10-19",
            age %in% 20:29 ~ "20-29",
            age %in% 30:39 ~ "30-39",
            age %in% 40:49 ~ "40-49",
            age %in% 50:59 ~ "50-59",
            age %in% 60:69 ~ "60-69",
            age %in% 70:79 ~ "70-79",
            age %in% 80:105 ~ "80+"
        )
    ) %>%
    filter(!date %in% max(date)) %>%
    drop_na()

saveRDS(data_individual, "data/data_individual.rds")

# Aggregated data (ECDC)

data_ecdc <-
    "https://opendata.ecdc.europa.eu/covid19/casedistribution/csv/data.csv" %>%
    read_csv() %>% as_tibble() %>%
    filter(countriesAndTerritories %in% "Croatia") %>%
    transmute(
        date = as.Date(dateRep, format = "%d/%m/%Y") - 1,
        deaths_new = deaths
    ) %>%
    arrange(date) %>%
    mutate(
        deaths_cum = cumsum(deaths_new),
        deaths_rollmean = zoo::rollmeanr(deaths_new, k = 7, fill = NA)
    ) %>%
    filter(deaths_cum > 0)

saveRDS(data_ecdc, "data/data_ecdc.rds")
