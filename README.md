# Simulating COVID-19 deaths from known confirmed cases

Using estimates of age-stratified CFR and time from confirmation to death, daily number of COVID-19 deaths can be approximately reconstructed by simple simulation.

